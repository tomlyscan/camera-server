from flask_wtf import Form
from wtforms import StringField, TextAreaField, BooleanField, SelectField,\
    SubmitField
from wtforms.validators import Required, Length, Email, Regexp
from wtforms import ValidationError
from flask_pagedown.fields import PageDownField
from ..models import Role, User


class NameForm(Form):
    name = StringField('Qual é o seu nome?', validators=[Required()])
    submit = SubmitField('Submit')


class EditProfileForm(Form):
    name = StringField('Nome real', validators=[Length(0, 64)])
    location = StringField('Localização', validators=[Length(0, 64)])
    about_me = TextAreaField('Sobre mim')
    submit = SubmitField('Submit')


class EditProfileAdminForm(Form):
    email = StringField('Email', validators=[Required(), Length(1, 64),
                                             Email()])
    username = StringField('Nome de Usuario', validators=[
        Required(), Length(1, 64), Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
                                          'Nomes de Usuário devem conter apenas letras, '
                                          'números, pontos ou sublinhados')])
    confirmed = BooleanField('Confirmado')
    role = SelectField('Role', coerce=int)
    name = StringField('Nome real', validators=[Length(0, 64)])
    location = StringField('Locatlização', validators=[Length(0, 64)])
    about_me = TextAreaField('Sobre mim')
    submit = SubmitField('Submit')

    def __init__(self, user, *args, **kwargs):
        super(EditProfileAdminForm, self).__init__(*args, **kwargs)
        self.role.choices = [(role.id, role.name)
                             for role in Role.query.order_by(Role.name).all()]
        self.user = user

    def validate_email(self, field):
        if field.data != self.user.email and \
                User.query.filter_by(email=field.data).first():
            raise ValidationError('Email já registrado.')

    def validate_username(self, field):
        if field.data != self.user.username and \
                User.query.filter_by(username=field.data).first():
            raise ValidationError('Nome de usuário em uso.')


class PostForm(Form):
    body = PageDownField("O que você tem em mente?", validators=[Required()])
    submit = SubmitField('Submit')


class CommentForm(Form):
    body = StringField('Digite seu comentário', validators=[Required()])
    submit = SubmitField('Submit')
