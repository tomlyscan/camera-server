#!/usr/bin/env python
from flask import Flask, render_template, Response

# Raspberry Pi camera module (requires picamera package)
#from camera_pi import Camera

# Emulated camera for debug
from camera import Camera

from flask_script import Manager
from flask_bootstrap import Bootstrap 

# Biblioteca de conexao raspberry com ponte h
#from AMSpi import AMSpi

server = Flask(__name__)

manager = Manager(server)
bootstrap = Bootstrap(server)

@server.route('/')
def index():
    """Video streaming home page."""
    return render_template('index.html')


def gen(camera):
    """Video streaming generator function."""
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

@server.route('/movement')
def movement(direction):
    """ Funcao que utiliza a AMSpi """


@server.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(Camera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    server.run(host='0.0.0.0', debug=True, threaded=True)
    # manager.run()
